<?php
return [
    'database' => [
        'runSeeder' => [
            'info' => [
                'search' => 'No seeder class specified. Searching from namespaces...',
                'found' => 'Seeder class found: {count}',
            ],
            'fail' => [
                'empty' => 'No seeder class available',
            ],
        ],
    ],
    'generator' => [
        'model' => [
            'prompt' => [
                'model' => 'Name of the model',
                'table' => 'Name of the table',
            ],
        ],
        'controller' => [
            'prompt' => [
                'controller' => 'Name of the controller',
                'useAuth' => 'Use AuthenticationTrait',
                'hasModel' => 'Use a model',
                'model' => 'Namespace of the model',
                'hasIndex' => "Use 'index' method",
                'hasShow' => "Use 'show' method",
                'hasNew' => "Use 'new' method",
                'hasCreate' => "Use 'create' method",
                'hasEdit' => "Use 'edit' method",
                'hasUpdate' => "Use 'update' method",
                'hasDelete' => "Use 'delete method'",
            ],
        ],
    ],
    'common' => [
        'fail' => [
            'write' => 'Error trying to create {0} file, check if the directory is writable.',
        ],
    ],
];
