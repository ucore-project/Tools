<?php

namespace uCore\Tools\Commands;

use CodeIgniter\CLI\CLI;

class BaseCommand extends \CodeIgniter\CLI\BaseCommand
{
    protected $group = 'uCore';

    public function run(array $params)
    {
        CLI::write("         ___               ", 'green');
        CLI::write(" _   _  / __\___  _ __ ___ ", 'green');
        CLI::write("| | | |/ /  / _ \| '__/ _ \\", 'green');
        CLI::write("| |_| / /__| (_) | | |  __/", 'green');
        CLI::write(" \__,_\____/\___/|_|  \___|", 'green');
        CLI::write();
        CLI::write('Welcome to uCore Tools', 'green');
        CLI::write();
    }
}
