<?php

namespace uCore\Tools\Commands\Database;

use CodeIgniter\CLI\CLI;
use uCore\Tools\Commands\BaseCommand;

class RunSeeder extends BaseCommand
{
    protected $name = "run:seeder";

    protected $description = "Run a seeder class.";

    protected $usage = "run:seeder [seeder_name]";

    protected $arguments = [
        'seeder_name' => 'The seeder name'
    ];

    protected $options = [];

    public function run(array $params)
    {
        parent::run($params);

        $seederName = array_shift($params);

        if (empty($seederName)) {
            CLI::write(lang('Tools.database.runSeeder.info.search'));

            $thead = ['ID', 'Namespace'];
            $tbody = [];

            $locator = \Config\Services::locator();
            $num = 0;
            foreach ($locator->listFiles('Database' . DIRECTORY_SEPARATOR . 'Seeds') as $file) {
                $className = $locator->getClassname($file);

                if ($className && is_subclass_of($className, 'CodeIgniter\Database\Seeder')) {
                    $num++;

                    array_push($tbody, [
                        $num,
                        $className
                    ]);
                }
            }

            if (empty($tbody)) {
                CLI::error(lang('Tools.database.runSeeder.fail.empty'));
                return;
            }

            CLI::write();

            CLI::write(lang('Tools.database.runSeeder.info.found', ['count' => CLI::color((string)count($tbody), 'green')]));
            CLI::table($tbody, $thead);

            CLI::write();

            $seederIndex = CLI::prompt('Select seeder ID', NULL, "required|numeric|greater_than_equal_to[1]|less_than_equal_to[{$num}]");
            $seederIndex--;

            $seederName = dot_search("{$seederIndex}.1", $tbody);
        }

        $seeder = \Config\Database::seeder();
        $seeder->call($seederName);
    }
}
