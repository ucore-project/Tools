<?php

namespace uCore\Tools\Commands\Generator;

use uCore\Tools\Commands\BaseCommand;
use CodeIgniter\CLI\CLI;
use CodeIgniter\Config\Services;

class CreateController extends BaseCommand
{

    protected $name = "generator:controller";

    protected $description = "Create a new controller file.";

    protected $usage = "generator:controller [controller_name] [Options]";

    protected $arguments = [
        'controller_name' => 'The controller file name'
    ];

    protected $options = [
        '-n' => 'Set controller namespace'
    ];

    public function run(array $params)
    {
        parent::run($params);

        helper('inflector');

        $data = [
            'namespace' => NULL,
            'controller' => array_shift($params),
            'useAuth' => 'n',
            'hasModel' => 'n',
            'model' => NULL,
            'hasIndex' => 'Y',
            'hasShow' => 'Y',
            'hasNew' => 'Y',
            'hasCreate' => 'Y',
            'hasEdit' => 'Y',
            'hasUpdate' => 'Y',
            'hasDelete' => 'Y',
        ];

        if (empty($data['controller'])) {
            $data['controller'] = CLI::prompt(lang('Tools.generator.controller.prompt.controller'), NULL, "required");
        }

        $data['useAuth'] = CLI::prompt(lang('Tools.generator.controller.prompt.useAuth'), ['n', 'Y']);

        if (($data['hasModel'] = CLI::prompt(lang('Tools.generator.controller.prompt.hasModel'), ['n', 'Y'])) == 'Y') {
            $data['model'] = CLI::prompt(lang('Tools.generator.controller.prompt.model'), NULL, "required");
        }

        foreach (['hasIndex', 'hasShow', 'hasNew', 'hasCreate', 'hasEdit', 'hasUpdate', 'hasDelete'] as $prop) {
            $data[$prop] = CLI::prompt(lang("Tools.generator.controller.prompt.{$prop}"), ['Y', 'n']);
        }

        $ns       = $params['-n'] ?? CLI::getOption('n');
        $homepath = APPPATH;

        if (!empty($ns)) {
            $namespaces = Services::autoloader()->getNamespace();

            foreach ($namespaces as $namespace => $path) {
                if ($namespace === $ns) {
                    $homepath = realpath(reset($path));
                    break;
                }
            }
        } else {
            $ns = 'App';
        }

        $data['namespace'] = $ns . '\\' . 'Controllers';
        $data['controller'] = pascalize($data['controller']);

        $fileName = $data['controller'] . ".php";
        $filePath = $homepath . DIRECTORY_SEPARATOR . 'Controllers' . DIRECTORY_SEPARATOR . $fileName;

        if (!is_dir(dirname($filePath))) {
            mkdir(dirname($filePath), 0777, TRUE);
        }

        $parser = \Config\Services::parser(__DIR__ . DIRECTORY_SEPARATOR . "Templates");
        $template = $parser->setData($data)->render('Controller.stub');

        helper('filesystem');

        if (!write_file($filePath, "<?php " . $template)) {
            CLI::error(lang('Tools.common.fail.write', [$path]));
            return;
        }

        CLI::write('Created file: ' . CLI::color(str_replace($homepath, $ns, $filePath), 'green'));
    }
}
