<?php

namespace uCore\Tools\Commands\Generator;

use uCore\Tools\Commands\BaseCommand;
use CodeIgniter\CLI\CLI;
use CodeIgniter\Config\Services;

class CreateModel extends BaseCommand
{

    protected $name = "generator:model";

    protected $description = "Create a new model file.";

    protected $usage = "generator:model [model_name] [Options]";

    protected $arguments = [
        'model_name' => 'The model file name'
    ];

    protected $options = [
        '-n' => 'Set model namespace'
    ];

    public function run(array $params)
    {
        parent::run($params);

        helper('inflector');

        $data = [
            'namespace' => NULL,
            'model' => array_shift($params),
            'table' => NULL,
        ];

        if (empty($data['model'])) {
            $data['model'] = CLI::prompt(lang('Tools.generator.model.prompt.model'), NULL, "required");
        }

        $data['table'] = CLI::prompt(lang('Tools.generator.model.prompt.table'), NULL, "required");

        $ns       = $params['-n'] ?? CLI::getOption('n');
        $homepath = APPPATH;

        if (!empty($ns)) {
            $namespaces = Services::autoloader()->getNamespace();

            foreach ($namespaces as $namespace => $path) {
                if ($namespace === $ns) {
                    $homepath = realpath(reset($path));
                    break;
                }
            }
        } else {
            $ns = 'App';
        }

        $data['namespace'] = $ns . '\\' . 'Models';
        $data['model'] = pascalize($data['model']);

        $fileName = $data['model'] . ".php";
        $filePath = $homepath . DIRECTORY_SEPARATOR . 'Models' . DIRECTORY_SEPARATOR . $fileName;

        if (!is_dir(dirname($filePath))) {
            mkdir(dirname($filePath), 0777, TRUE);
        }

        $parser = \Config\Services::parser(__DIR__ . DIRECTORY_SEPARATOR . "Templates");
        $template = $parser->setData($data)->render('Model.stub');

        helper('filesystem');

        if (!write_file($filePath, "<?php " . $template)) {
            CLI::error(lang('Tools.common.fail.write', [$path]));
            return;
        }

        CLI::write('Created file: ' . CLI::color(str_replace($homepath, $ns, $filePath), 'green'));
    }
}
