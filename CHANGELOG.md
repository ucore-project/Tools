# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 1.0.0 (2020-08-21)


### Features

* add uCore\Tools ([f160c0a](https://gitlab.com/ucore-project/Tools/commit/f160c0a7ad6e19d6ae52cc0cce6afbadfb727b99))
